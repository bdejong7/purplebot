﻿using Discord;
using Discord.Legacy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace PurpleBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool reactToCommands;
        public List<Command> commands = new List<Command>();
        public List<Channel> purpleList = new List<Channel>();
        public List<ulong> grayList = new List<ulong>();
        public List<string> enteredCommands = new List<string>();
        public DispatcherTimer timer = new DispatcherTimer();
        private DiscordClient Bot;

        public Channel lastChannel;
        public MainWindow()
        {
            InitializeComponent();

            Bot = new DiscordClient();

            Bot.MessageReceived += Bot_MessageReceived;
            Bot.Connect("teampurplebot@tutanota.com", password);
            commands = Load("basic.txt", true);
            LoadBlackList();
        }

        private void Bot_MessageReceived(object sender, MessageEventArgs e)
        {
            lastChannel = e.Channel;
            User[] mentiondUsers = e.Message.MentionedUsers.ToArray();
            Channel[] mentiondChannels = e.Message.MentionedChannels.ToArray();
            if (grayList.Contains(e.Channel.Id)) return;
            if (e.Message.IsAuthor) return;
            lastChannel = e.Channel;
            if (!reactToCommands) return;
            bool isCommand = false;

            for (int i = 0; i < commands.Count; i++)
            {
                string answer = commands[i].answer.Replace("<user>", e.User.Mention);
                string message = e.Message.Text.Replace(commands[i].command + " ", "");
                switch (commands[i].type)
                {
                    case CommandType.equal:
                        if (commands[i].command.ToLower() == e.Message.Text.ToLower())
                        {
                            SendMessage(answer, message, e.Channel, mentiondUsers);
                            isCommand = true;
                        }
                        break;
                    case CommandType.contains:
                        if (e.Message.Text.ToLower().Contains(commands[i].command))
                        {
                            SendMessage(answer, message, e.Channel, mentiondUsers);
                            isCommand = true;
                        }
                        break;
                    default:
                        if (e.Message.Text.ToLower().StartsWith(commands[i].command))
                        {
                            SendMessage(answer, message, e.Channel, mentiondUsers);
                            isCommand = true;
                        }
                        break;
                }
            }

            if (e.Message.Text == "!noice")
            {
                for (int i = 0; i < 15; i++)
                {
                    e.Channel.SendMessage("https://www.youtube.com/watch?v=a8c5wmeOL9o");
                    isCommand = true;
                }
            }

            if (e.Message.Text == "!reload")
            {
                e.Channel.SendMessage(" *- Checking PurpleBot.cs for updates.*");
                e.Channel.SendMessage(" *- PurpleBot.cs was successfully reloaded!*");
                isCommand = true;
                commands = Load("basic.txt", true);
                LoadBlackList();
            }

            if (e.Message.Text == "!swipe")
            {
                ClearChat(e.Channel, 10);
                isCommand = true;
            }

            if (e.Message.Text.StartsWith("!insult"))
            {
                string user = e.Message.Text.Replace("!insult ", "");
                e.Channel.SendMessage("*" + insults[new Random().Next(insults.Length - 1)].Replace("<username>", user) + "*");
                isCommand = true;
            }

            if (e.Message.Text.StartsWith("!"))
            {
                e.Message.Delete();
                if (!isCommand)
                {
                    e.Channel.SendMessage("*This is not a valid command!!*");
                }
            }
            if (isCommand)
            {
                enteredCommands.Add(e.Message.Text + ". entered by: " + e.User.Name + " in channel: " + e.Server + "/" + e.Channel);
            }
        }

        private void SendMessage(string message, string command, Channel chan, User[] mentiontUser)
        {
            if (message.Contains("<clear>"))
            {
                ClearChat(chan, 1000000000);
                message = message.Replace("<clear>", "");
            }
            if (mentiontUser.Length != 0)
            {
                if (message.Contains("<@>"))
                {
                    message = message.Replace("<@>", mentiontUser[0].Mention);
                }
            }
            else message = message.Replace("<@>", "");
            if (message.Contains("<cheer>"))
            {
                message = message.Replace("<cheer>", "");
                message = Cheer(command);
            }
            chan.SendMessage("*" + message + "*");
        }

        private void ClearChat(Channel chan, int amount)
        {
            chan.DownloadMessages(amount);
            foreach (Message item in chan.Messages)
            {
                item.Delete();
            }
            foreach (Message item in chan.Messages)
            {
                item.Delete();
            }
            chan.SendMessage(" *- Chat cleaned.*");
        }

        private string Cheer(string text)
        {
            text = text.ToUpper() + "!";
            text = text + " " + text + " " + text;
            return text;
        }

        public void LoadBlackList()
        {
            List<string> stringd = GetList(@"C:\PurpleBot\Commands\com\BlackList.txt");
            grayList.Clear();
            for (int i = 0; i < stringd.Count; i++)
            {
                grayList.Add(Convert.ToUInt64(stringd[i]));
            }
        }

        public List<Command> Load(string fileName, bool downloadNewCommands)
        {
            List<Command> commands = new List<Command>();

            string path = (@"C:\PurpleBot\Commands\com\" + fileName);

            if (downloadNewCommands)
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile("http://pastebin.com/raw/hFYDvCs2", path);
            }

            List<string> strings = GetList(path);

            for (int i = 0; i < strings.Count; i++)
            {
                strings[i] = strings[i].Replace("/", Environment.NewLine);
                CommandType type;
                switch (strings[i].Split('|')[1])
                {
                    case "0":
                        type = CommandType.contains;
                        break;
                    case "1":
                        type = CommandType.equal;
                        break;
                    case "2":
                        type = CommandType.startsWith;
                        break;
                    default:
                        type = CommandType.equal;
                        break;
                }

                strings[i] = strings[i].Replace("|" + strings[i].Split('|')[1], "");
                string[] text = strings[i].Split('^');
                commands.Add(new Command(text[0], text[1], type));
            }
            return commands;
        }

        public List<string> GetList(string path)
        {
            try
            {
                List<string> strings = new List<string>();
                StreamReader sr = new StreamReader(path);
                string line;

                line = sr.ReadLine();
                while (line != null)
                {
                    strings.Add(line);
                    line = sr.ReadLine();
                }
                sr.Close();
                return strings;
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception: " + e.Message);
            }
            return null;
        }


        private void Close(object sender, RoutedEventArgs e)
        {
            foreach (Server item in Bot.Servers)
            {
                foreach (Channel chan in item.TextChannels)
                {
                    chan.SendMessage("*PurpleBot is shutting down!*");
                }
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            foreach (Server item in Bot.Servers)
            {
                foreach (Channel chan in item.TextChannels)
                {
                    //chan.SendMessage("*PurpleBot[Discord] has successfully launched!*");
                    chan.SendIsTyping();
                }
            }
            Bot.SetGame("innocent.");
        }

        public string[] insults = new string[]
        {
            "You're a rotten tomato, <username>!",
            "You're a boring apple, <username>!",
            "You're a stinky broccoli, <username>!",
            "You're a weak banana, <username>!",
            "You're a lame potato, <username>!",
            "You're a sloppy burger, <username>!",
            "You're a salty cucumber, <username>!",
        };

        private void react_Checked(object sender, RoutedEventArgs e)
        {
            reactToCommands = react.IsChecked.Value;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (listBox.SelectedIndex < 0) return;
            Channel chan = purpleList[listBox.SelectedIndex];
            chan.SendMessage(message.Text);
        }

        private void ManualReload_Click(object sender, RoutedEventArgs e)
        {
            commands = Load("basic.txt", false);
        }

        private void RefreshChannels_Click(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            purpleList.Clear();
            LoadBlackList();
            foreach (Server item in Bot.Servers)
            {
                foreach (Channel chan in item.TextChannels)
                {
                    if (!grayList.Contains(chan.Id))
                    {
                        listBox.Items.Add(new ListBoxItem().Content = chan.Name);
                        purpleList.Add(chan);
                    }
                    else Blacklistje.Items.Add(chan.Id);
                }
            }
        }

        private void AddToBlackList_Click(object sender, RoutedEventArgs e)
        {
            Blacklistje.Items.Add(new ListBoxItem().Content = purpleList[listBox.SelectedIndex].Id);

            Clipboard.SetText(purpleList[listBox.SelectedIndex].Id.ToString());

            grayList.Add(purpleList[listBox.SelectedIndex].Id);

            listBox.Items.RemoveAt(listBox.SelectedIndex);
        }

        private void RefreshCommands_Click(object sender, RoutedEventArgs e)
        {
            CalledCommands.Items.Clear();
            for (int i = 0; i < enteredCommands.Count; i++)
            {
                CalledCommands.Items.Add(enteredCommands[i]);
            }
        }































































































        public string password = "Purpl3B0tPassword";


    }
}
