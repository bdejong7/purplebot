﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurpleBot
{
    public class Command
    {
        public string command;
        public string answer;
        public CommandType type;

        public Command(string command, string answer, CommandType type)
        {
            this.command = command;
            this.answer = answer;
            this.type = type;
        }

        public bool CheckCommand(string message)
        {
            return true;
        }
    }
}
